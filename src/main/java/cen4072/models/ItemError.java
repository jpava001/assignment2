package cen4072.models;

public class ItemError {
    public String error;

    public ItemError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
