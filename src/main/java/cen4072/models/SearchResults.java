package cen4072.models;

import java.util.List;

public class SearchResults {
    private List<Book> local;
    private List<Book> interlibrary;

    public SearchResults(List<Book> local, List<Book> interlibrary) {
        this.local = local;
        this.interlibrary = interlibrary;
    }

    public List<Book> getLocal() {
        return local;
    }

    public void setLocal(List<Book> local) {
        this.local = local;
    }

    public List<Book> getInterlibrary() {
        return interlibrary;
    }

    public void setInterlibrary(List<Book> interlibrary) {
        this.interlibrary = interlibrary;
    }
}
