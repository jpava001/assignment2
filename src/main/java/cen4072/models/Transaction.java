package cen4072.models;

public class Transaction {
    int quantity;

    private Transaction() {

    }

    public Transaction(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
