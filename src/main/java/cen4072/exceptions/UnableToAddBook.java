package cen4072.exceptions;

public class UnableToAddBook extends Exception {
    public UnableToAddBook(String msg) {
        super(msg);
    }
}
