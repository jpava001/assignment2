package cen4072.exceptions;

public class UnableToCheckoutBook extends Exception {
    public UnableToCheckoutBook(String msg) {
        super(msg);
    }
}
