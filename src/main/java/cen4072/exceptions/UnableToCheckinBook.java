package cen4072.exceptions;

public class UnableToCheckinBook extends Exception {
    public UnableToCheckinBook(String msg) {
        super(msg);
    }
}
