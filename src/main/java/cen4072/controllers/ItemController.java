package cen4072.controllers;

import cen4072.exceptions.UnableToAddBook;
import cen4072.exceptions.UnableToCheckinBook;
import cen4072.exceptions.UnableToCheckoutBook;
import cen4072.models.Book;
import cen4072.models.ItemError;
import cen4072.models.SearchResults;
import cen4072.models.Transaction;
import cen4072.services.ItemService;
import cen4072.services.ItemServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ItemController {
    ItemService itemService;

    public ItemController() {
        itemService = new ItemServiceImpl();
    }

    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }

    public ResponseEntity getAllItems(String author,String title) {
        if(author != null) {
            SearchResults items = itemService.getItemsByAuthor(author);

            if(items.getLocal().isEmpty() && items.getInterlibrary().isEmpty()) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }

            return new ResponseEntity(items, HttpStatus.OK);
        }

        if(title != null) {
            SearchResults items = itemService.getItemsByTitle(title);

            if(items.getLocal().isEmpty() && items.getInterlibrary().isEmpty()) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }

            return new ResponseEntity(items, HttpStatus.OK);
        }

        SearchResults allItems = itemService.getAllItems();
        return new ResponseEntity(allItems, HttpStatus.OK);
    }

    public ResponseEntity checkoutItem(int id, Transaction transaction) {
        try {
            itemService.checkoutBook(id, transaction);
        } catch (UnableToCheckoutBook e) {
            ItemError error = new ItemError(e.getMessage());
            return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    public ResponseEntity checkinItem(int id, Transaction transaction) {
        try {
            itemService.checkinBook(id, transaction);
        } catch (UnableToCheckinBook e) {
            ItemError error = new ItemError(e.getMessage());
            return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    public ResponseEntity addBook(Book book) {
        try {
            itemService.addBook(book);
        } catch(UnableToAddBook e) {
            ItemError error = new ItemError(e.getMessage());
            return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(HttpStatus.OK);
    }
}
