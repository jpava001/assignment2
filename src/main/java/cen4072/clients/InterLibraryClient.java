package cen4072.clients;

import cen4072.models.Book;

import java.util.List;

public interface InterLibraryClient {
    List<Book> getAllInterLibraryBooks();
    List<Book> getAllItemsByAuthor(String author);
    Book getItemByTitle(String title);
}
