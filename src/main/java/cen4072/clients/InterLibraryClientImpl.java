package cen4072.clients;

import cen4072.models.Book;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import org.apache.http.HttpStatus;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class InterLibraryClientImpl implements InterLibraryClient {
    public InterLibraryClientImpl(String baseUri, int port) {
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri(baseUri)
                .setPort(port)
                .setContentType(ContentType.JSON)
                .build();
    }

    public List<Book> getAllInterLibraryBooks() {
        Response response = RestAssured
                .get("items")
                .thenReturn();

        List<Book> books = Arrays.asList(response.getBody().as(Book[].class));

        return books;
    }

    public List<Book> getAllItemsByAuthor(String author) {
        Response response = RestAssured.
                given().param("author", author)
                .get("items")
                .thenReturn();

        if(response.statusCode() == HttpStatus.SC_NOT_FOUND) {
            return Collections.emptyList();
        }

        List<Book> books = Arrays.asList(response.getBody().as(Book[].class));

        return books;
    }

    public Book getItemByTitle(String title) {
        Response response = RestAssured.
                given().param("title", title)
                .get("items")
                .thenReturn();

        if(response.statusCode() == HttpStatus.SC_NOT_FOUND) {
            return null;
        }

        Book book = response.getBody().as(Book.class);

        return book;
    }
}
