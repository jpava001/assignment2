package cen4072.repositories;

import cen4072.models.Book;

import java.util.List;

public interface ItemRepository {
    void initialize();
    void addBook(Book book);
    List<Book> findAllBooks();
    List<Book> findBookByAuthor(String author);
    Book findBookByTitle(String title);
    Book findBookById(int id);
    void updateAvailable(int id, int available);
}
