package cen4072.repositories;

import cen4072.models.Book;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class H2ItemRepository implements ItemRepository {
    private static final String DB_DRIVER = "org.h2.Driver";
    private static final String DB_CONNECTION = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
    private static final String DB_USER = "";
    private static final String DB_PASSWORD = "";

    public void initialize() {
        createSchema();

        addBook(
            new Book(
                "the cat in the hat",
                "seuss",
                1
            )
        );

        addBook(
            new Book(
                "oh the places you'll go",
                "seuss",
                3
            )
        );

        addBook(
            new Book(
                "harry potter and the philosopher's stone",
                "rowling",
                8
            )
        );

        addBook(
            new Book(
                "harry potter and the prisoner of azkaban",
                "rowling",
                0
            )
        );

        addBook(
            new Book(
                "introduction to software testing",
                "ammann",
                5
            )
        );
    }

    @Override
    public void addBook(Book book) {
        Connection connection = null;
        PreparedStatement insertQuery = null;
        ResultSet rs = null;

        try {
            connection = getDBConnection();
            insertQuery = connection.prepareStatement("INSERT INTO ITEMS(title, author, available) values (?, ?, ?)");
            insertQuery.setString(1, book.getTitle());
            insertQuery.setString(2, book.getAuthor());
            insertQuery.setInt(3, book.getAvailable());

            insertQuery.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, insertQuery, rs);
        }
    }

    @Override
    public List<Book> findAllBooks() {
        List<Book> results = new LinkedList<Book>();
        Connection connection = null;
        PreparedStatement selectQuery = null;
        ResultSet rs = null;

        try {
            connection = getDBConnection();
            selectQuery = connection.prepareStatement("SELECT id, title, author, available FROM ITEMS");

            rs = selectQuery.executeQuery();
            while(rs.next()) {
                results.add(
                        new Book(
                                rs.getInt("id"),
                                rs.getString("title"),
                                rs.getString("author"),
                                rs.getInt("available")
                        )
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, selectQuery, rs);
        }

        return results;
    }

    @Override
    public List<Book> findBookByAuthor(String author) {
        List<Book> results = new LinkedList<Book>();
        Connection connection = null;
        PreparedStatement selectQuery = null;
        ResultSet rs = null;

        try {
            connection = getDBConnection();
            selectQuery = connection.prepareStatement("SELECT id, title, author, available FROM ITEMS WHERE author=?");
            selectQuery.setString(1, author);

            rs = selectQuery.executeQuery();
            while(rs.next()) {
                results.add(
                    new Book(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getString("author"),
                        rs.getInt("available")
                    )
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, selectQuery, rs);
        }

        return results;
    }

    @Override
    public Book findBookByTitle(String title) {
        Book result = null;
        Connection connection = null;
        PreparedStatement selectQuery = null;
        ResultSet rs = null;

        try {
            connection = getDBConnection();
            selectQuery = connection.prepareStatement("SELECT id, title, author, available FROM ITEMS WHERE title=?");
            selectQuery.setString(1, title);

            rs = selectQuery.executeQuery();
            while(rs.next()) {
                result = new Book(
                            rs.getInt("id"),
                            rs.getString("title"),
                            rs.getString("author"),
                            rs.getInt("available")
                        );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, selectQuery, rs);
        }

        return result;
    }

    @Override
    public Book findBookById(int id) {
        Book result = null;
        Connection connection = null;
        PreparedStatement selectQuery = null;
        ResultSet rs = null;

        try {
            connection = getDBConnection();
            selectQuery = connection.prepareStatement("SELECT id, title, author, available FROM ITEMS WHERE id=?");
            selectQuery.setInt(1, id);

            rs = selectQuery.executeQuery();
            while(rs.next()) {
                result = new Book(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getString("author"),
                        rs.getInt("available")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, selectQuery, rs);
        }

        return result;
    }

    @Override
    public void updateAvailable(int id, int available) {
        Connection connection = null;
        PreparedStatement updateQuery = null;
        ResultSet rs = null;

        try {
            connection = getDBConnection();
            updateQuery = connection.prepareStatement("UPDATE ITEMS SET available=? WHERE id=?");
            updateQuery.setInt(1, available);
            updateQuery.setInt(2, id);

            updateQuery.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, updateQuery, rs);
        }
    }

    private void createSchema() {
        Connection connection = null;
        PreparedStatement createQuery = null;
        ResultSet rs = null;

        try {
            connection = getDBConnection();
            createQuery = connection.prepareStatement("CREATE TABLE ITEMS(id int primary key auto_increment, title varchar(255), author varchar(255), available int)");

            createQuery.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, createQuery, rs);
        }
    }

    private static Connection getDBConnection() {
        Connection dbConnection = null;
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try {
            dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
            return dbConnection;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dbConnection;
    }

    private static void close(AutoCloseable... closeables) {
        for(AutoCloseable closeable : closeables) {
            try {
                closeable.close();
            } catch(Exception e) {
                // do nothing
            }
        }
    }
}
