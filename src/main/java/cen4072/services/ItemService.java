package cen4072.services;

import cen4072.exceptions.UnableToAddBook;
import cen4072.exceptions.UnableToCheckinBook;
import cen4072.exceptions.UnableToCheckoutBook;
import cen4072.models.Book;
import cen4072.models.SearchResults;
import cen4072.models.Transaction;

import java.util.List;

public interface ItemService {
    SearchResults getAllItems();
    SearchResults getItemsByAuthor(String author);
    SearchResults getItemsByTitle(String title);
    void addBook(Book book) throws UnableToAddBook;
    void checkoutBook(int id, Transaction transaction) throws UnableToCheckoutBook;
    void checkinBook(int id, Transaction transaction) throws UnableToCheckinBook;
}
