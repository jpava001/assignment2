package cen4072.services;

import cen4072.clients.InterLibraryClientImpl;
import cen4072.exceptions.UnableToAddBook;
import cen4072.exceptions.UnableToCheckinBook;
import cen4072.exceptions.UnableToCheckoutBook;
import cen4072.models.Book;
import cen4072.models.SearchResults;
import cen4072.models.Transaction;
import cen4072.repositories.H2ItemRepository;
import cen4072.repositories.ItemRepository;

import java.util.ArrayList;
import java.util.List;

public class ItemServiceImpl implements ItemService {
    private ItemRepository itemRepository;
    private InterLibraryClientImpl interLibraryClient;

    public ItemServiceImpl() {
        itemRepository = new H2ItemRepository();
        itemRepository.initialize();

        interLibraryClient = new InterLibraryClientImpl("http://cen4072.eastus.cloudapp.azure.com", 8081);
    }

    public ItemServiceImpl(ItemRepository itemRepository, InterLibraryClientImpl interLibraryClient) {
        this.itemRepository = itemRepository;
        this.itemRepository.initialize();

        this.interLibraryClient = interLibraryClient;
    }

    public SearchResults getAllItems() {
        List<Book> localBooks = itemRepository.findAllBooks();
        List<Book> remoteBooks = interLibraryClient.getAllInterLibraryBooks();

        return new SearchResults(localBooks, remoteBooks);
    }

    @Override
    public SearchResults getItemsByAuthor(String author) {
        List<Book> localBooks = itemRepository.findBookByAuthor(author);
        List<Book> remoteBooks = interLibraryClient.getAllItemsByAuthor(author);

        return new SearchResults(localBooks, remoteBooks);
    }

    @Override
    public SearchResults getItemsByTitle(String title) {
        Book localBook = itemRepository.findBookByTitle(title);
        Book remoteBook = interLibraryClient.getItemByTitle(title);

        List<Book> localBooks = new ArrayList<Book>();
        localBooks.add(localBook);

        List<Book> remoteBooks = new ArrayList<Book>();
        remoteBooks.add(remoteBook);

        return new SearchResults(localBooks, remoteBooks);
    }

    @Override
    public void addBook(Book book) throws UnableToAddBook {
        if(book.getTitle() == null || book.getTitle().isEmpty()) {
            throw new UnableToAddBook("Title must be provided");
        }
        if(book.getAuthor() == null || book.getAuthor().isEmpty()) {
            throw new UnableToAddBook("Author must be provided");
        }

        Book existingBook = itemRepository.findBookByTitle(book.getTitle());

        if(existingBook != null) {
            throw new UnableToAddBook("Book title already exists.");
        }

        itemRepository.addBook(book);
    }

    @Override
    public void checkoutBook(int id, Transaction transaction) throws UnableToCheckoutBook {
        if(transaction.getQuantity() <= 0) {
            throw new UnableToCheckoutBook("Quantity must be positive");
        }

        Book existingBook = itemRepository.findBookById(id);
        if(existingBook == null) {
            throw new UnableToCheckoutBook("Book does not exist");
        }

        if(transaction.getQuantity() > existingBook.getAvailable()) {
            throw  new UnableToCheckoutBook("Not enough to checkout");
        }

        itemRepository.updateAvailable(id, existingBook.getAvailable() - transaction.getQuantity());
    }

    @Override
    public void checkinBook(int id, Transaction transaction) throws UnableToCheckinBook {
        if(transaction.getQuantity() <= 0) {
            throw new UnableToCheckinBook("Quantity must be positive");
        }

        Book existingBook = itemRepository.findBookById(id);
        if(existingBook == null) {
            throw new UnableToCheckinBook("Book does not exist");
        }

        itemRepository.updateAvailable(id, existingBook.getAvailable() + transaction.getQuantity());
    }
}
